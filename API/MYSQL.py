from fastapi import FastAPI
import mysql.connector
from pydantic import BaseModel

def get_database():
    # MySQL connection parameters
    db_config = {
        'host': 'localhost',
        'user': 'root',
        'password': 'root',
        'database': 'db_api'
        # 'auth_plugin': 'mysql_native_password'
    }

    # Create a MySQL connection using mysql-connector-python
    connection = mysql.connector.connect(**db_config)
    return connection

conn = get_database()

class NewClient(BaseModel):
    first_name: str
    last_name: str
    email: str
    order_count: int

class EditClient(BaseModel):
    email: str
    first_name: str = None
    last_name: str = None
    order_count: int = None

class DeleteClient(BaseModel):
    email: str

app = FastAPI()

@app.post("/newClient")
async def newClient(new: NewClient):
    cursor = conn.cursor(dictionary=True)
    query = "INSERT INTO clients (first_name, last_name, email, order_count) VALUES (%s, %s, %s, %s)"
    values = (new.first_name, new.last_name, new.email, new.order_count)
    cursor.execute(query, values)
    conn.commit()
    cursor.close()
    return {"message": "Client added", "client": new}

@app.post("/editClient")
async def editClient(edit: EditClient):
    cursor = conn.cursor(dictionary=True)
    query = "UPDATE clients SET first_name = %s, last_name = %s, order_count = %s WHERE email = %s"
    values = (edit.first_name, edit.last_name, edit.order_count, edit.email)
    cursor.execute(query, values)
    conn.commit()
    cursor.close()
    return {"message": "Client edited", "client": edit}

@app.post("/deleteClient")
async def deleteClient(delete: DeleteClient):
    cursor = conn.cursor(dictionary=True)
    query = "DELETE FROM clients WHERE email = %s"
    values = (delete.email,)
    cursor.execute(query, values)
    conn.commit()
    cursor.close()
    return {"message": "Client deleted", "client": delete}

@app.get("/getClient")
async def getClient(email: str):
    cursor = conn.cursor(dictionary=True)
    query = "SELECT * FROM clients WHERE email = %s"
    values = (email,)
    cursor.execute(query, values)
    client = cursor.fetchone()
    cursor.close()
    return {"message": "Client found", "client": client}
