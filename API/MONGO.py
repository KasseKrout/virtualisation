from fastapi import FastAPI
from pymongo import MongoClient
from pydantic import BaseModel


def get_database():
 
   # Provide the mongodb atlas url to connect python to mongodb using pymongo
   CONNECTION_STRING = "mongodb://localhost:27017/"
   
   # Create a connection using MongoClient. You can import MongoClient or use pymongo.MongoClient
   client = MongoClient(CONNECTION_STRING)
 
   # Create the database for our example (we will use the same database throughout the tutorial
   return client['carParts']

db = get_database()
collection = db['parts']

# Car parts
class NewPart(BaseModel):
    name: str
    description: str
    price: float
    stock: int

class EditPart(BaseModel):
    name: str
    description: str = None
    price: float = None
    stock: int = None
    
class DeletePart(BaseModel):
    name: str

    
app = FastAPI()

# Car parts queries
@app.post("/newPart")
async def newPart(new: NewPart):
    mongopart = {
        "name": new.name,
        "description": new.description,
        "price": new.price,
        "stock": new.stock
    }
    collection.insert_one(mongopart)
    return {"message": "Part added", "part": new}

@app.post("/editPart")
async def editPart(edit: EditPart):
    mongopart = {
        "name": edit.name,
        "description": edit.description,
        "price": edit.price,
        "stock": edit.stock
    }
    if(edit.description == None):
        del mongopart["description"]
    if(edit.price == None):
        del mongopart["price"]
    if(edit.stock == None):
        del mongopart["stock"]
    
    collection.update_one({"name": edit.name}, {"$set": mongopart})
    return {"message": "Part edited", "part": edit}

@app.post("/deletePart")
async def deletePart(delete: DeletePart):
    collection.delete_one({"name": delete.name})
    return {"message": "Part deleted", "part": delete}

@app.get("/getPart")
async def getPart(name: str):
    part = collection.find_one({"name": name}, {'_id': 0})
    return {"message": "Part found", "part": part}




    